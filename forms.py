from django import forms
from django.forms.models import inlineformset_factory, BaseModelFormSet, BaseInlineFormSet
from django.utils.translation import ugettext_lazy as _
from .models import Workspace, Task


class WorkspaceEditForm(forms.ModelForm):
    class Meta:
        model = Workspace
        fields = ['title', 'slug', 'access',]

class TaskEditForm(forms.ModelForm):
    # TODO: Add support for role management
    # TODO: Add support for related task management
    #parent_task = forms.ModelChoiceField()
    class Meta:
        model = Task
        fields = ['title', 'parent', 'slug', 'complete_if', 'kind', 'status', 'priority', 'workload', 'completion', 'schedule', 'start_date', 'due_date', 'interval',]
