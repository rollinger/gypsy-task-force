from django.conf.urls import url
from django.views.generic import RedirectView
from .views import (GTFMainView, WorkspaceReadView, WorkspaceCreateView,
WorkspaceUpdateView, WorkspaceDeleteView, TaskReadView, TaskCreateView, TaskUpdateView, TaskDeleteView)

urlpatterns = [
    # GTF Redirect no url Page
    url(r'^$',RedirectView.as_view(pattern_name='gtf_workspace_overview', permanent=False)),
    # GTF Index Start Page
    url(r'^workspace/$',
        GTFMainView.as_view(),
        name='gtf_workspace_overview'),
    #
    # Workspace CRUD URL Endpoints
    #
    # GTF Create Workspace View
    url(r'^workspace/new/$',
        WorkspaceCreateView.as_view(),
        name='workspace_create_view'),
    # GTF Read Workspace View <slug> blank->request.user
    url(r'^workspace/view/(?P<workspace_slug>[-\w]+)/$',
        WorkspaceReadView.as_view(),
        name='workspace_read_view'),
    # GTF Update Workspace View
    url(r'^workspace/update/(?P<workspace_slug>[-\w]+)/$',
        WorkspaceUpdateView.as_view(),
        name='workspace_update_view'),
    # GTF Delete Workspace View
    url(r'^workspace/delete/(?P<workspace_slug>[-\w]+)/$',
        WorkspaceDeleteView.as_view(),
        name='workspace_delete_view'),

    #
    # Task CRUD URL Endpoints
    #
    # GTF Create Task View
    url(r'^workspace/(?P<workspace_slug>[-\w]+)/task/new/$',
        TaskCreateView.as_view(),
        name='task_create_view'),
    # GTF Read Task View <task-slug>
    url(r'^workspace/(?P<workspace_slug>[-\w]+)/task/(?P<task_slug>[-\w]+)/$',
        TaskReadView.as_view(),
        name='task_read_view'),
    # GTF Update Task View
    url(r'^workspace/(?P<workspace_slug>[-\w]+)/task/update/(?P<task_slug>[-\w]+)/$',
        TaskUpdateView.as_view(),
        name='task_update_view'),
    # GTF Delete Task View
    url(r'^workspace/(?P<workspace_slug>[-\w]+)/task/delete/(?P<task_slug>[-\w]+)/$',
        TaskDeleteView.as_view(),
        name='task_delete_view'),
]
