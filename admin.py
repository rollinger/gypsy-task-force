from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from django.utils.translation import ugettext_lazy as _
from .models import Task, TaskEvent, TaskRole, Workspace


class TaskRoleInline(admin.TabularInline):
    model = TaskRole
    fk_name = 'task'
    extra = 1
class TaskEventHistoryInline(admin.TabularInline):
    model = TaskEvent
    verbose_name_plural = _('Task Event History')
    fk_name = 'task'
    extra = 0
    readonly_fields = ('user', 'message', 'created_at',)
    ordering = ('-created_at',)
    def has_add_permission(self, request):
        return False
class AddTaskEventInline(admin.TabularInline):
    model = TaskEvent
    verbose_name_plural = _('Add Task Event to History')
    fk_name = 'task'
    extra = 1
    fields = ('user', 'message',)
    def has_change_permission(self, request, obj=None):
        return False
class TaskAdmin(MPTTModelAdmin):
    list_display = ('title', 'workspace', 'kind', 'status','priority','workload', "completion")
    list_filter = ('kind', 'status','priority',"schedule")
    search_fields = ('title','complete_if')
    #ordering = ('-due_date','-created_at',)
    #autocomplete_fields = ()
    inlines = [TaskRoleInline,TaskEventHistoryInline,AddTaskEventInline]
    readonly_fields = ('created_at','updated_at','slug',)
admin.site.register(Task, TaskAdmin)



class TaskEventAdmin(admin.ModelAdmin):
    list_display = ('task', 'user', 'message')
    #list_filter = ('',)
    search_fields = ('task__title',)
    ordering = ('-created_at',)
    readonly_fields = ('created_at','updated_at')
admin.site.register(TaskEvent, TaskEventAdmin)

class TaskRoleAdmin(admin.ModelAdmin):
    list_display = ('user', 'task', 'role')
    list_filter = ('role',)
    search_fields = ('task__title','task__complete_if')
    ordering = ('-created_at',)
    readonly_fields = ('created_at','updated_at')
admin.site.register(TaskRole, TaskRoleAdmin)

class WorkspaceAdmin(admin.ModelAdmin):
    list_display = ('user', 'title', 'access', 'slug')
    list_filter = ('access',)
    #search_fields = ('task__title','task__complete_if')
    #ordering = ('-created_at',)
    readonly_fields = ('created_at','updated_at')#, 'slug')
admin.site.register(Workspace, WorkspaceAdmin)
