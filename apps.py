from django.apps import AppConfig


class GypsyTodoConfig(AppConfig):
    name = 'gypsy_todo'
    verbose_name = "Gypsy Task Force"
