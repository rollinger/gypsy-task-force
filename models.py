"""
Gypsy Task Force: Model Definitions
"""

from django.db import models
from mptt.models import MPTTModel, TreeManager, TreeForeignKey
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.validators import MaxValueValidator, MinValueValidator
from django.template.defaultfilters import slugify
import itertools

WORKSPACE_ACCESS_LEVELS = (
    ('0', _('Private')),
    ('1', _('Public')),
)

class WorkspaceManager(models.Manager):
    pass
class Workspace(models.Model):
    """
    Workspace for Tasks
    """
    # Model Attribute Definition
    user        = models.ForeignKey(settings.AUTH_USER_MODEL,
                related_name="%(app_label)s_%(class)s_for_user",
                help_text=_('Owner of the Workspace'))
    title       = models.CharField(_("Workspace Title"),
                max_length=255,
                help_text=_('Set the title of the Workspace.'))
    slug        = models.SlugField(_('Slug'),
                null=True, blank=True, max_length=150, unique=True, editable=True,
                help_text=_('URL Fragment to access the Workspace'))
    # Workspace Settings
    # TODO: Describe what each Setting will do
    access      = models.CharField(_("Access Level of Workspace"),
                max_length=255, choices=WORKSPACE_ACCESS_LEVELS, default=0,
                help_text=_('Set to Private to hides this workspace from public read access'))

    # TODO: Add general Settings to that Workspace (inline or FK-Model?)

    # Timestamps
    created_at          = models.DateTimeField(_("Created At"), auto_now_add=True)
    updated_at          = models.DateTimeField(_("Updated At"), auto_now=True)

    # Object Manager
    objects = WorkspaceManager()

    # Model Method Definition

    def __str__(self):
        """
        Conversion to a String
        """
        return _('%s (%s)') % (self.title, self.user)

    # Calculated Values
    # TODO: Cache and precalculate those data

    def metadata(self):
        meta = self.tasks.aggregate(
            models.Avg('completion'),
            models.Sum('workload'),
            models.Count('id'),
            )
        #print(meta)
        return meta

    # URL Methods

    def get_absolute_url(self):
        """
        Returns the Absolute READ URL of Workspace
        """
        return reverse('workspace_read_view', kwargs={'workspace_slug': self.slug})
    def get_absolute_create_url(self):
        """
        Returns the Absolute CREATE URL of Workspace
        """
        return reverse('workspace_create_view')
    def get_absolute_update_url(self):
        """
        Returns the Absolute UPDATE URL of Workspace
        """
        return reverse('workspace_update_view', kwargs={'workspace_slug': self.slug})
    def get_absolute_delete_url(self):
        """
        Returns the Absolute DELETE URL of Workspace
        """
        return reverse('workspace_delete_view', kwargs={'workspace_slug': self.slug})

    def generate_slug(self):
        """
        Generates and sets the slug if not set already or the title changes
        """
        if self.slug == "" or self.slug is None:
            self.slug = orig = slugify(self.__str__())
            for x in itertools.count(1):
                if not Workspace.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

    def save(self, *args, **kwargs):
        # Generate slug
        self.generate_slug()
        super(Workspace, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Workspace')
        verbose_name_plural = _('Workspace')
        ordering = ['created_at',]



TASK_TYPE = (
    ('0', _('Task')),
    ('1', _('Goal')),
    ('2', _('Project')),
)
TASK_STATUS = (
    ('0', _('Planned')),
    ('1', _('Started')),
    ('2', _('Halted')),
    ('3', _('Aborted')),
    ('4', _('Completed')),
)
TASK_PRIORITY = (
    ('1', _('Low')),
    ('2', _('Normal')),
    ('3', _('High')),
)
TASK_SCHEDULE = (
    ('0', _('Not Scheduled')),
    ('1', _('Scheduled')),
    ('3', _('Repetetive')),
)

class TaskManager(TreeManager):
    pass
class Task(MPTTModel):
    """
    Task Model:
    a task can be a Goal, a Project, a Task or Subtask/Follow-Up
    """
    # Model Attribute Definition
    title           = models.CharField(_('Title'),
                    max_length=255,
                    help_text=_('Choose a well formed title for this Task, Goal or Project.'))
    slug            = models.SlugField(_('Slug'),
                    null=True, blank=True, max_length=150, unique=True, editable=True,
                    help_text=_('URL Fragment to access the Task'))
    complete_if     = models.TextField(_("Completion Condition"),
                    max_length=500,
                    help_text=_('Define the conditions that must be met for task completion.'))

    # Tree Model by MPTT https://github.com/django-mptt/django-mptt
    parent          = TreeForeignKey('self',
                    null=True, blank=True, related_name='children', db_index=True)

    # Task Collaboration/ Participants is realized by TaskRole Relational Model
    workspace       = models.ForeignKey(Workspace,
                    related_name='%(class)ss', null=True, blank=True, editable=True,
                    help_text=_('A Task belongs to a workspace'))

    # Task Process Data
    kind        = models.CharField(_('Type of Task'),
                max_length=255, choices=TASK_TYPE, default='0',
                help_text=_('Set the kind of the ToDo Item (Task, Goal or Project).'))
    status      = models.CharField(_('Status of Task'),
                max_length=255, choices=TASK_STATUS, default='0',
                help_text=_('State of Task (Workflow Status).'))
    priority    = models.CharField(_('Task Priority'),
                max_length=255, choices=TASK_PRIORITY, default='2',
                help_text=_('Set the priority of the Task (Low, Normal, High).'))
    # TODO: Add automated workload calculation for Goals and Projects if set in the settings
    workload    = models.DecimalField(_('Workload of Task'),
                max_digits=12, decimal_places=2, default=0.0,
                help_text=_('Set the estimated workload of this todo item.'))
    completion  = models.PositiveIntegerField(_('Progress of Task'),
                default=0, validators=[MinValueValidator(0), MaxValueValidator(100)],
                help_text=_('Set the estimated completion percentage (0-100%).'))

    # Task Scheduling
    schedule    = models.CharField(_('Type of Schedule'),
                max_length=255, choices=TASK_SCHEDULE, default='0',
                help_text=_('If set to Scheduled or Repetetive then start and due date has to be set.'))
    start_date  = models.DateTimeField(_('Start Date'),
                null=True, blank=True,
                help_text=_('The Date and Time when this Task can be worked on.'))
    due_date    = models.DateTimeField(_('Due Date'),
                null=True, blank=True,
                help_text=_('The Date and Time when this Task needs to be finished.'))
    # TODO: Think about repetitive Task and interval and make it more intuitive
    interval    = models.DecimalField(_('Interval of repetetive Task in Hours'),
                max_digits=12, decimal_places=2, default=0.0, null=True, blank=True,
                help_text=_('If schedule in set to Repetetive the interval in hours from Due Date when this Task will be restarted'))

    # TODO: Add meta data and calculated data, like hours_worked_sofar, activity, forsaken(due_date& hours_worked-workload) time_till_deadline, time_till_start, etc... decide whether it should be persistent or calculated

    # Timestamps
    created_at          = models.DateTimeField(_("Created At"), auto_now_add=True)
    updated_at          = models.DateTimeField(_("Updated At"), auto_now=True)

    # Object Manager
    objects = TaskManager()

    # Model Method Definition

    def __str__(self):
        """
        Conversion to a String
        """
        return _('%s') % (self.title)

    def get_absolute_url(self):
        """
        Returns the Absolute URL of Task
        """
        return reverse('task_read_view', kwargs={'workspace_slug': self.workspace.slug,
        'task_slug': self.slug})
    def get_absolute_create_url(self):
        """
        Returns the Absolute CREATE URL of Task
        """
        return reverse('task_create_view', kwargs={'workspace_slug': self.workspace.slug})
    def get_absolute_update_url(self):
        """
        Returns the Absolute UPDATE URL of Task
        """
        return reverse('task_update_view', kwargs={'workspace_slug': self.workspace.slug,
        'task_slug': self.slug})
    def get_absolute_delete_url(self):
        """
        Returns the Absolute DELETE URL of Task
        """
        return reverse('task_delete_view', kwargs={'workspace_slug': self.workspace.slug,
        'task_slug': self.slug})

    def generate_slug(self):
        """
        Generates and sets the slug if not set already
        """
        if self.slug == "" or self.slug is None:
            self.slug = orig = slugify(self.title)
            for x in itertools.count(1):
                if not Task.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

    def save(self, *args, **kwargs):
        # Generate slug
        self.generate_slug()
        super(Task, self).save(*args, **kwargs)

    class MPTTMeta:
        order_insertion_by = ['priority','title']

    class Meta:
        verbose_name = _('Task')
        verbose_name_plural = _('Tasks')



USER_TASK_ROLE = (
    ('0', _('Observer')),
    ('1', _('Editor')),
    ('2', _('Author')),
    ('3', _('Owner')),
)
class TaskRoleManager(models.Manager):
    pass
class TaskRole(models.Model):
    """
    Task Role of a User for the Task
    """
    # From Task
    task        = models.ForeignKey(Task,
                related_name="%(app_label)s_%(class)s_for_task",
                help_text=_('Related Task for the Role'))
    user        = models.ForeignKey(settings.AUTH_USER_MODEL,
                related_name="%(app_label)s_%(class)s_for_user",
                help_text=_('Related User holding the Role'))
    role        = models.CharField(_("Type of Task Role"),
                max_length=255, choices=USER_TASK_ROLE,
                help_text=_('Set the type of role for the user on that task'))

    # Timestamps
    created_at          = models.DateTimeField(_("Created At"), auto_now_add=True)
    updated_at          = models.DateTimeField(_("Updated At"), auto_now=True)

    # Object Manager
    objects = TaskRoleManager()

    # Model Method Definition

    def __str__(self):
        """
        Conversion to a String
        """
        return _("%s is %s of %s") % (self.user, self.get_role_display(), self.task)

    class Meta:
        verbose_name = _('Task Role')
        verbose_name_plural = _('Task Roles')
        #ordering = ['start_date', 'due_date']



TASK_MESSAGES_TYPE = (
    ('0', _('Standard')),
    ('1', _('Custom')),
)
TASK_STANDARD_MESSAGES = (
    ('0', _('')),
    ('1', _('')),
)
class TaskEventManager(models.Manager):
    pass
class TaskEvent(models.Model):
    """
    Task Event
    """
    # From Task
    task        = models.ForeignKey(Task,
                related_name="%(app_label)s_%(class)s_for_task",
                help_text=_('Related Task for the Event Message'))
    user        = models.ForeignKey(settings.AUTH_USER_MODEL,
                related_name="%(app_label)s_%(class)s_from_user",
                help_text=_('Related User for the Event Message'))
    message     = models.TextField(_("Event Message"),
                max_length=500,
                help_text=_('Standard or custom event message for a task'))

    # Timestamps
    created_at          = models.DateTimeField(_("Created At"), auto_now_add=True)
    updated_at          = models.DateTimeField(_("Updated At"), auto_now=True)

    # Object Manager
    objects = TaskEventManager()

    # Model Method Definition

    def __str__(self):
        """
        Conversion to a String
        """
        return _("%s on %s: %s") % (self.user, self.task, self.message)

    class Meta:
        verbose_name = _('Task Message')
        verbose_name_plural = _('Task Messages')
        #ordering = ['start_date', 'due_date']
