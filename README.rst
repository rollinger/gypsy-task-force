Gypsy Task Force
----------------
Gypsy Task Force is more than a ToDo-List: It is a Full Fledge Collaboration Application that incorporates functionalities from Project Management, Time Tracking and Ticket Systems.

Quick start
-----------

1. Install dependencies:
    # https://github.com/django-mptt/django-mptt
    pip install django-mptt

2. Add "mptt" and "gypsy_todo" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'mptt'
        'gypsy_todo',
    ]

3. Include the gypsy_todo URLconf in your project urls.py like this::

    url(r'^todo/', include('gypsy_todo.urls')),

4. Run `python manage.py migrate` to create the gypsy_todo models.

5. Start the development server and visit http://127.0.0.1:8000/todo/



Project(Goal)
    |_  Goal
        |_  ToDo
            |_ ToDo
        |_  ToDo
    |_  Goal
        |_  ToDo    >   ToDo
        |               |_  ToDo
        |_  ToDo

##############
### Models ###
##############
Workspace:

Task: TreeModel
    workspace
    parent

TaskRole
    User > Task

TaskEvent
    User > Task


##############
### IDEAS: ###
##############
OK   Project,Goals&Tasks belong to a Workspace (private/public)
O   Task can have individual Workflow Settings (or inherit the settings from the workspace)
O   users have settings, workhours per day, working in streches or chunks of x min, start working hour /days, bio, skills(rated, and hours worked with that skill), etc...
O   Score Points for completion of tasks
    => maybe later paypal integraton...
O   TaskParticipation User can send applications
O   Nice Alarm system (currently working on Tx?) (not too invasive)
    Generate Priority/due date work schedule for the day
    Makes work recommendations (takes pauses into consideration)
    Ask about concentration/motivation
O   E-Mailing System with customizable Templates
O   multi users & Gypsy Task Force User Profile Extention
    OK  different users can colaborate on same task (read,write,admin
O   TaskTicket: All Participants can add a Ticket that gets voted, discussed and approved/disproved by owner => Turns into new ToDo Item (Goal, Task...)
O   Add Documentation to Codebase https://docs.djangoproject.com/en/1.11/ref/contrib/admin/admindocs/
O   Model delete cascade
O   TaskEvent:
    O   Set Standard Task Events
    O   interpolate the following into the message text field(1000char)
    <user> has made the following Changes to the <task_kind>: <task_title>:
    <field>: <old_value> => <new_value>
    hook it on task save
