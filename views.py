from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import get_list_or_404, get_object_or_404
from django.utils.translation import ugettext as _
from django.views.generic import View, TemplateView, RedirectView, FormView, DetailView, UpdateView, DeleteView, CreateView, ListView
from .models import Workspace, Task
from .forms import WorkspaceEditForm, TaskEditForm



class GTFMainView(ListView):
    model = Workspace
    template_name = 'gypsy_task_force/main_view_page.html'
    def get(self, request):
        workspaces = Workspace.objects.filter(user=request.user)
        return render(request, self.template_name, {
            'user_workspaces': workspaces,
        })

#
# Workspace Views
#

class WorkspaceReadView(DetailView):
    """
    Workspace Detail View
    id = workspace_slug
    """
    template_name = 'gypsy_task_force/workspace/detail_view_page.html'
    def get(self, request, workspace_slug):
        workspace = Workspace.objects.get(slug=workspace_slug)
        other_workspaces = Workspace.objects.filter(user=request.user)
        return render(request, self.template_name, {
            'workspace': workspace,
            'other_workspaces': other_workspaces,
        })

class WorkspaceCreateView(CreateView):
    """
    Creates a Workspace for the User
    """
    template_name = 'gypsy_task_force/workspace/create_view_page.html'
    model = Workspace
    form_class = WorkspaceEditForm
    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return HttpResponseRedirect( obj.get_absolute_url() )

class WorkspaceUpdateView(UpdateView):
    """
    Updates a Workspace for the User
    """
    template_name = 'gypsy_task_force/workspace/update_view_page.html'
    model = Workspace
    slug_url_kwarg = 'workspace_slug'
    form_class = WorkspaceEditForm

class WorkspaceDeleteView(DeleteView):
    """
    Delete the Workspace
    TODO: In Model delete tasks cascadingly
    """
    model = Workspace
    slug_url_kwarg = 'workspace_slug'
    template_name = 'gypsy_task_force/workspace/workspace_delete_view.html'
    success_url = reverse_lazy('gtf_workspace_overview')

#
# Task Views
#

class TaskReadView(DetailView):
    """
    Task Detail View
    slug_url_kwarg = 'task_slug'
    """
    template_name = 'gypsy_task_force/task/detail_view_page.html'

    def get(self, request, workspace_slug, task_slug):
        task = Task.objects.get(slug=task_slug)
        #parent = Workspace.objects.filter(user=request.user)
        #child = Workspace.objects.filter(user=request.user)
        return render(request, self.template_name, {
            'task': task,
            #'other_workspaces': other_workspaces,
        })

class TaskCreateView(CreateView):
    """
    Creates a Task in a given Workspace for the User
    """
    template_name = 'gypsy_task_force/task/task_create_view_page.html'
    model = Task
    form_class = TaskEditForm

    def get_workspace(self):
        return get_object_or_404(Workspace, slug=self.kwargs['workspace_slug'])

    def get_initial(self):
        initial = super(CreateView, self).get_initial()
        #initial['workspace'] = self.get_workspace()
        return initial

    def get_form(self, form_class):
        form = super(CreateView, self).get_form(form_class)
        #form.fields['parent_task'].queryset = Task.objects.all()#filter()
        return form

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.workspace = get_object_or_404(Workspace, slug=self.kwargs['workspace_slug'])
        obj.save()
        return HttpResponseRedirect( obj.get_absolute_url() )

class TaskUpdateView(UpdateView):
    """
    Updates a Tasks
    """
    template_name = 'gypsy_task_force/task/task_update_view_page.html'
    model = Task
    slug_url_kwarg = 'task_slug'
    form_class = TaskEditForm

class TaskDeleteView(DeleteView):
    """
    Delete the Task
    TODO: In Model delete tasks relation and role cascadingly
    """
    model = Task
    slug_url_kwarg = 'task_slug'
    template_name = 'gypsy_task_force/task/task_delete_view.html'

    def get_success_url(self):
        # Return to the Workspace View that task belonged to.
        return reverse_lazy('workspace_read_view', kwargs={'workspace_slug': self.kwargs['workspace_slug']})
