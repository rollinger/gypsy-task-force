# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-09-30 06:34
from __future__ import unicode_literals

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gypsy_todo', '0002_auto_20170929_0902'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message', models.TextField(help_text='Standard or custom event message for a task', max_length=500, verbose_name='Event Message')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated At')),
            ],
            options={
                'verbose_name': 'Task Message',
                'verbose_name_plural': 'Task Messages',
            },
        ),
        migrations.CreateModel(
            name='TaskRelation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kind', models.CharField(choices=[(b'0', 'Subtask'), (b'1', 'Follow-up Task')], default=b'0', help_text='', max_length=255, verbose_name='Type of Task Relation')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated At')),
            ],
            options={
                'verbose_name': 'Task Relation',
                'verbose_name_plural': 'Task Relations',
            },
        ),
        migrations.AlterModelOptions(
            name='task',
            options={'ordering': ['start_date', 'due_date'], 'verbose_name': 'Task', 'verbose_name_plural': 'Tasks'},
        ),
        migrations.AddField(
            model_name='task',
            name='completion',
            field=models.PositiveIntegerField(default=0, help_text='', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Progress of Task'),
        ),
        migrations.AddField(
            model_name='task',
            name='coworker',
            field=models.ManyToManyField(help_text='', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='task',
            name='due_date',
            field=models.DateTimeField(blank=True, help_text='', null=True, verbose_name='Due Date'),
        ),
        migrations.AddField(
            model_name='task',
            name='interval',
            field=models.DecimalField(blank=True, decimal_places=2, default=0.0, help_text='', max_digits=12, null=True, verbose_name='Interval of repetetive Task'),
        ),
        migrations.AddField(
            model_name='task',
            name='priority',
            field=models.CharField(choices=[(b'1', 'Low'), (b'2', 'Normal'), (b'3', 'High')], default=b'1', help_text='Set the priority of the Task (Low, Normal, High)', max_length=255, verbose_name='Task Priority'),
        ),
        migrations.AddField(
            model_name='task',
            name='schedule',
            field=models.CharField(choices=[(b'0', 'Not Scheduled'), (b'1', 'Scheduled'), (b'3', 'Repetetive')], default=b'0', help_text='', max_length=255, verbose_name='Type of Schedule'),
        ),
        migrations.AddField(
            model_name='task',
            name='start_date',
            field=models.DateTimeField(blank=True, help_text='', null=True, verbose_name='Start Date'),
        ),
        migrations.AddField(
            model_name='task',
            name='workload',
            field=models.DecimalField(decimal_places=2, default=0.0, help_text='', max_digits=12, verbose_name='Workload of Task'),
        ),
        migrations.AlterField(
            model_name='task',
            name='complete_if',
            field=models.TextField(help_text='Define the conditions when the task is completed', max_length=500, verbose_name='Completion Condition'),
        ),
        migrations.AlterField(
            model_name='task',
            name='kind',
            field=models.CharField(choices=[(b'0', 'Task'), (b'1', 'Goal'), (b'2', 'Project')], default=b'0', max_length=255, verbose_name='Type of Task'),
        ),
        migrations.AlterField(
            model_name='task',
            name='owner',
            field=models.ForeignKey(help_text='', on_delete=django.db.models.deletion.CASCADE, related_name='gypsy_todo_task', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.CharField(choices=[(b'0', 'Planned'), (b'1', 'Started'), (b'2', 'Halted'), (b'3', 'Aborted'), (b'4', 'Completed')], default=b'0', max_length=255, verbose_name='Status of Task'),
        ),
        migrations.AddField(
            model_name='taskrelation',
            name='source',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='taskrelation_outbound', to='gypsy_todo.Task'),
        ),
        migrations.AddField(
            model_name='taskrelation',
            name='target',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='taskrelation_inbound', to='gypsy_todo.Task'),
        ),
        migrations.AddField(
            model_name='taskevent',
            name='task',
            field=models.ForeignKey(help_text='', on_delete=django.db.models.deletion.CASCADE, to='gypsy_todo.Task'),
        ),
        migrations.AddField(
            model_name='taskevent',
            name='user',
            field=models.ForeignKey(help_text='', on_delete=django.db.models.deletion.CASCADE, related_name='gypsy_todo_taskevent', to=settings.AUTH_USER_MODEL),
        ),
    ]
